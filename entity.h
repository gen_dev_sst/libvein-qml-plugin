#ifndef ENTITY_H
#define ENTITY_H

#include <QQuickItem>
#include <veinentity.h>

class Entity : public QQuickItem
{
  Q_OBJECT
  Q_ENUMS(VeinEntity::Modifiers)
  Q_PROPERTY(QString name READ name)
  Q_PROPERTY(QUrl src READ src WRITE setSrc NOTIFY srcChanged)
  Q_PROPERTY(QVariant value READ value WRITE setValue NOTIFY valueChanged)
  Q_PROPERTY(bool noecho READ noecho WRITE setNoecho)
  Q_PROPERTY(bool ready READ ready NOTIFY readyChanged)
  Q_PROPERTY(bool socketErrorNotify READ socketErrorNotify WRITE setSocketErrorNotify NOTIFY socketErrorNotifyChanged)
public:

  explicit Entity(QQuickItem *parent = 0);

  bool noecho();
  QString name();
  QVariant value();

  void setNoecho(bool noechoFlag);
  void setValue(QVariant var);

  const QUrl &src();
  void setSrc(const QUrl &url);

  void remoteUrlResolved(VeinEntity *ent);
  bool ready() const;

  bool socketErrorNotify() const
  {
    return m_socketErrorNotify;
  }

signals:
  void valueChanged(QVariant newValue);
  void socketError();
  //void modifiersChanged(QList<VeinEntity::Modifiers> newModifiers);

  void readyChanged(bool arg);

  void srcChanged(QUrl arg);

  void socketErrorNotifyChanged(bool socketErrorNotify);

public slots:
  void reloadEntitySystem();

  void setSocketErrorNotify(bool socketErrorNotify);

private slots:
  void initConnections();
  void onSocketError();

protected:
  void componentComplete() Q_DECL_OVERRIDE;

private slots:
  void onInitialValueChanged(QVariant newValue);
private:
  /// @todo PIMPL
  VeinEntity *entity;
  QUrl entityUrl;

  //  QList<QVariant> valueQueue;
  bool m_ready;
  QUrl m_src;
  bool m_socketErrorNotify;
};

#endif // ENTITY_H
