#ifndef PEER_H
#define PEER_H

#include <QQuickItem>

class Entity;
class VeinPeer;
class Hub;

class Peer : public QQuickItem
{
  Q_OBJECT
  Q_PROPERTY(QString name READ name)
  Q_PROPERTY(bool remote READ isRemote)
public:
  explicit Peer(QQuickItem *parent = 0);

  Entity *getEntity(QString name);
  QString name() const;
  bool isRemote();

  Entity *dataAdd(QString name);
  void dataRemove(Entity *entity);

  Hub *getHub();

signals:

private:
  VeinPeer *peer;
};

#endif // PEER_H
