#include "veinqml_plugin.h"
#include "hub.h"
#include "peer.h"
#include "entity.h"
#include "entitymodel.h"
#include "zfftbarchart.h"

#include <qqml.h>

void VeinQMLPlugin::registerTypes(const char *uri)
{
  // @uri Vein
  qmlRegisterType<Entity>(uri, 1, 0, "Entity");
  qmlRegisterType<EntityModel>(uri, 1, 0, "EntityModel");
  qmlRegisterType<ZFFTBarChart>(uri, 1, 0, "ZFFTBarChart");
}


