#include "entity.h"

#include "veinservice.h"
#include <veinpeer.h>
#include <QDebug>



Entity::Entity(QQuickItem *parent) :
  QQuickItem(parent),
  entity(0),
  m_ready(false),
  m_socketErrorNotify(false)
{
  initConnections();
}

bool Entity::noecho()
{
  if(entity)
  {
    return entity->getModifiers().contains(VeinEntity::MOD_NOECHO);
  }
  else
  {
    return 0;
  }
}

QString Entity::name()
{
  if(entity)
  {
    return entity->getName();
  }
  else
    return QString();
}

QVariant Entity::value()
{
  if(entity)
  {
    return entity->getValue();
  }
  else
    return QVariant(0);
}

void Entity::setNoecho(bool noechoFlag)
{
  if(entity&& !entity->getOwner()->isRemote())
  {
    if(noechoFlag)
    {
      entity->modifiersAdd(VeinEntity::MOD_NOECHO);
    }
    else
    {
      entity->modifiersRemove(VeinEntity::MOD_NOECHO);
    }
  }
}

void Entity::setValue(QVariant var)
{
  /// @note only sets the value if the component is ready
  if(m_ready && entity && isComponentComplete())
  {
    entity->setValue(var);
  }
}

const QUrl &Entity::src()
{
  return entityUrl;
}

void Entity::setSrc(const QUrl &url)
{
  if(entity)
  {
    //qWarning() << "[vein-qml] Entity.src is const after it has been written once";
  }
  else
  {
    if(url.isValid() && url.scheme().toLower()=="vein" && url.hasFragment())
    {
      entityUrl=url;
      emit srcChanged(url);
      entity=VeinService::getInstance()->resolveURL(entityUrl);
      if(entity)
      {
        connect(entity,SIGNAL(sigValueChanged(QVariant)),this,SIGNAL(valueChanged(QVariant)));
        if(entity->getValue()!=QVariant::Invalid)
        {
          m_ready=true;
          readyChanged(m_ready);
          valueChanged(entity->getValue());
        }
        else
        {
         connect(entity,SIGNAL(sigValueChanged(QVariant)),this,SLOT(onInitialValueChanged(QVariant)));
         entity->fetchEntityData();
        }
      }
      else
      {
        VeinService::getInstance()->addWaiting(this);
      }
    }
  }
}

void Entity::remoteUrlResolved(VeinEntity *ent)
{
  if(ent)
  {
    entity=ent;
    connect(entity,SIGNAL(sigValueChanged(QVariant)),this,SIGNAL(valueChanged(QVariant))); // move this connect to onInitialValueChanged() ?
    if(entity->getValue()!=QVariant::Invalid)
    {
      m_ready=true;
      readyChanged(m_ready);
      valueChanged(entity->getValue());
    }
    else
    {
     connect(entity,SIGNAL(sigValueChanged(QVariant)),this,SLOT(onInitialValueChanged(QVariant)));
     ///@todo should this be done automagically from the sigValueChanged connectNotifier?
     entity->fetchEntityData();
    }
    //    //if the value was changed before the network has been resolved
    //    for(int i=0; i<valueQueue.count();) // i++ would be an error here, better change this to a foreach loop
    //    {
    //      entity->setValue(valueQueue.at(i));
    //      valueQueue.removeAt(i);
    //    }
  }
}

bool Entity::ready() const
{
  return m_ready;
}

void Entity::reloadEntitySystem()
{
  disconnect(VeinService::getInstance(),SIGNAL(sigSocketError()),this,SIGNAL(socketError()));
  VeinService::deconstructSingleton();
  initConnections();
}

void Entity::setSocketErrorNotify(bool socketErrorNotify)
{
  if (m_socketErrorNotify == socketErrorNotify)
    return;
  m_socketErrorNotify = socketErrorNotify;
  emit socketErrorNotifyChanged(socketErrorNotify);
  if(socketErrorNotify)
  {
    initConnections();
  }

}

void Entity::initConnections()
{
  if(m_socketErrorNotify)
  {
    connect(VeinService::getInstance(),SIGNAL(sigSocketError()),this,SLOT(onSocketError()));
    qDebug() << "[VeinQML]INIT" << VeinService::getInstance();
  }
}

void Entity::onSocketError()
{
  VeinService::deconstructSingleton();
  initConnections();
  emit socketError();
}

void Entity::componentComplete()
{
  QQuickItem::componentComplete();
  if(entityUrl.isEmpty())
  {
    //qWarning() << "[vein-qml] Entity.src is empty";
  }
}

void Entity::onInitialValueChanged(QVariant newValue)
{
  if(!m_ready)// && newValue.isValid())
  {
    disconnect(this,SLOT(onInitialValueChanged(QVariant)));
    m_ready=true;
    readyChanged(m_ready);
//    qDebug() << QTime::currentTime()  << "Entity"<<entityUrl<<"becomes ready";
    valueChanged(newValue);
  }
}
