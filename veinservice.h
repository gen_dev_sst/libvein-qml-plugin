#ifndef VEINURLRESOLVER_H
#define VEINURLRESOLVER_H

#include <QObject>
#include <QList>

class Entity;
class VeinEntity;
class VeinHub;
class VeinTcpController;

class VeinService : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY(VeinService)

public:
  VeinEntity *resolveURL(const QUrl &url);
  VeinHub *resolveURLToHub(const QUrl &url);

  static VeinService *getInstance();
  static void deconstructSingleton();

  void addWaiting(Entity *ent);

  VeinTcpController *getController();

public slots:
  /**
   * @brief Resolves waiting Entity instances that requested a networked VeinHub
   */
  void onHubConnected(VeinHub *remoteHub);
  void onSocketError();

signals:
  void sigHubConnected(VeinHub *remoteHub);
  void sigSocketError();
private:
  explicit VeinService(QObject *qobjParent = 0);
  ~VeinService();

  bool checkWaiting(QUrl url);

  VeinTcpController *controller;

  static VeinService *singletonInstance;

  QList<Entity *> networkWaiting;
};

#endif // VEINURLRESOLVER_H
