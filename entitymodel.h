#ifndef ENTITYMODEL_H
#define ENTITYMODEL_H

#include <QAbstractListModel>
#include <QHash>
#include <QUrl>

#include <veinentity.h>
class VeinHub;

class EntityModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(QUrl src READ src WRITE setSrc NOTIFY srcChanged)

public:
  enum QmlRoles {
    EntName = Qt::UserRole + 1,
    EntModifiers,
    EntValue,
    EntValueType,
    PeerName
  };

  EntityModel(QObject *qObjParent = 0);

  QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
  QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
  int rowCount(const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;

  QUrl src() const;

public slots:
  void setSrc(QUrl url);

private slots:
  void onHubConnected(VeinHub *newHub);
  void onEntValueChanged();
  void onEntModifiersChanged();
  void onEntAdded();
  void onEntRemoved();

signals:
  void srcChanged(QUrl arg);

private:
  void alignDataTable();
  QStringList prettyModifierNames(QList<VeinEntity::Modifiers> mods) const;

  QHash<int, VeinEntity*> dataTable;
  QUrl m_src;
  VeinHub *m_hub;
};

#endif // ENTITYMODEL_H
