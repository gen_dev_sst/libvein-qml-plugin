#ifndef ZFFTBARCHART_H
#define ZFFTBARCHART_H

#include <QQuickPaintedItem>
#include <QList>
#include <QColor>
#include <QString>

class QwtPlot;
class ZBarData;
class QwtPlotCanvas;
class VeinEntity;


/**
 * @brief Zera specific crap class to display hyperstyled FFT bar charts
 * @todo delete this pile of crap
 */
class ZFFTBarChart : public QQuickPaintedItem
{
  Q_OBJECT
  Q_DISABLE_COPY(ZFFTBarChart)

  Q_PROPERTY(bool bottomLabels READ bottomLabels WRITE setBottomLabels NOTIFY labelsChanged)
  Q_PROPERTY(bool legendEnabled READ legendEnabled WRITE setLegendEnabled)
  Q_PROPERTY(QColor borderColor READ borderColor WRITE setborderColor NOTIFY borderColorChanged)
  Q_PROPERTY(QColor color READ bgColor WRITE setBgColor NOTIFY bgColorChanged)
  Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor)

  Q_PROPERTY(QString chartTitle READ chartTitle WRITE setChartTitle)

  //left axis
  Q_PROPERTY(bool logScaleLeftAxis READ logScaleLeftAxis WRITE setLogScaleLeftAxis)
  Q_PROPERTY(double maxValueLeftAxis READ maxValueLeftAxis WRITE setMaxValueLeftAxis)
  Q_PROPERTY(double minValueLeftAxis READ minValueLeftAxis WRITE setMinValueLeftAxis)
  Q_PROPERTY(QColor colorLeftAxis READ colorLeftAxis WRITE setColorLeftAxis)
  Q_PROPERTY(QString titleLeftAxis READ titleLeftAxis WRITE setTitleLeftAxis)
  Q_PROPERTY(QUrl leftValueSource READ leftValueSource WRITE setLeftValueSource NOTIFY leftValueSourceChanged)

  //right axis
  Q_PROPERTY(bool logScaleRightAxis READ logScaleRightAxis WRITE setLogScaleRightAxis)
  Q_PROPERTY(double maxValueRightAxis READ maxValueRightAxis WRITE setMaxValueRightAxis)
  Q_PROPERTY(double minValueRightAxis READ minValueRightAxis WRITE setMinValueRightAxis)
  Q_PROPERTY(QColor colorRightAxis READ colorRightAxis WRITE setColorRightAxis)
  Q_PROPERTY(bool rightAxisEnabled READ rightAxisEnabled WRITE setRightAxisEnabled)
  Q_PROPERTY(QString titleRightAxis READ titleRightAxis WRITE setTitleRightAxis)
  Q_PROPERTY(QUrl rightValueSource READ rightValueSource WRITE setRightValueSource NOTIFY rightValueSourceChanged)

public:
  ZFFTBarChart(QQuickItem *parent = 0);
  ~ZFFTBarChart();


  bool bottomLabels() const;
  bool legendEnabled() const;

  QColor bgColor() const;
  QColor borderColor() const;
  QColor textColor() const;
  QString chartTitle() const;

  // QQmlParserStatus interface
  void componentComplete() Q_DECL_OVERRIDE;
  void paint(QPainter *painter) Q_DECL_OVERRIDE;

  /**
    * @b redeclared to prevent calling QQuickItem::classBegin()
    */
  void classBegin() Q_DECL_OVERRIDE {}

  //left axis
  bool logScaleLeftAxis() const;
  double maxValueLeftAxis() const;
  double minValueLeftAxis() const;
  QColor colorLeftAxis() const;
  QString titleLeftAxis() const;
  QUrl leftValueSource() const;

  //right axis
  bool logScaleRightAxis() const;
  double maxValueRightAxis() const;
  double minValueRightAxis() const;
  QColor colorRightAxis() const;
  bool rightAxisEnabled() const;
  QString titleRightAxis() const;
  QUrl rightValueSource() const;

public slots:
  void onExternValuesChanged();
  void onHeightChanged();
  void onWidthChanged();
  void setBgColor(QColor arg);
  void setborderColor(QColor arg);
  void setBottomLabels(bool arg);
  void setChartTitle(QString arg);
  void setLegendEnabled(bool arg);
  void setTextColor(QColor arg);

  //left axis
  void setLogScaleLeftAxis(bool arg);
  void setMaxValueLeftAxis(double arg);
  void setMinValueLeftAxis(double arg);
  void setColorLeftAxis(QColor arg);
  void setTitleLeftAxis(QString arg);
  void setLeftValueSource(const QUrl &leftValueSource);

  //right axis
  void setLogScaleRightAxis(bool arg);
  void setMaxValueRightAxis(double arg);
  void setMinValueRightAxis(double arg);
  void setColorRightAxis(QColor arg);
  void setRightAxisEnabled(bool arg);
  void setTitleRightAxis(QString arg);
  void setRightValueSource(const QUrl &rightValueSource);

signals:
  void bgColorChanged(QColor arg);
  void borderColorChanged(QColor arg);
  void labelsChanged(QStringList arg);
  void maxValueLeftAxisChanged(double arg);
  void minValueChanged(double arg);
  void leftValueSourceChanged(QUrl leftValueSource);
  void rightValueSourceChanged(QUrl rightValueSource);

private slots:
  void onExternValuesChangedTimeout();
  void onLabelsChanged(QStringList arg);
  void onRefreshTimeout();
  void refreshPlot();
  void onLeftValueChanged(QVariant leftValue);
  void onRightValueChanged(QVariant rightValue);
  void onLeftBarCountChanged(int barCount);

private:
  bool m_bottomLabelsEnabled;
  bool m_legendEnabled;

  QColor m_bgColor;
  QColor m_borderColor;
  QColor m_textColor;
  QStringList m_bottomLabels;
  QString m_chartTitle;
  QTimer *m_refreshTimer;
  QTimer *m_valuesTimer;
  QwtPlotCanvas *m_canvas;
  QwtPlot *m_plot;

  //left axis
  bool m_logScaleLeftAxis;
  ZBarData *m_barDataLeft;
  double m_maxValueLeftAxis;
  double m_minValueLeftAxis;
  QList<qreal> m_valuesLeftAxis;
  VeinEntity *m_leftEntity;
  QUrl m_leftValueSource;
  QColor m_colorLeftAxis;
  int m_leftBarCount;

  //right axis
  bool m_logScaleRightAxis;
  ZBarData *m_barDataRight;
  double m_maxValueRightAxis;
  double m_minValueRightAxis;
  QList<qreal> m_valuesRightAxis;
  VeinEntity *m_rightEntity;
  QUrl m_rightValueSource;
  QColor m_colorRightAxis;

};

#endif // ZFFTBARCHART_H
