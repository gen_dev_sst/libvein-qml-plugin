#include "entitymodel.h"

#include "veinservice.h"

#include <veintcpcontroller.h>
#include <veinhub.h>
#include <veinpeer.h>
#include <veinentity.h>
#include <protonetpeer.h>
#include <QDebug>


Q_DECLARE_METATYPE(QStringList)

EntityModel::EntityModel(QObject *qObjParent)
  : QAbstractListModel(qObjParent)
{
}

QHash<int, QByteArray> EntityModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  roles[EntName] = "name";
  roles[EntModifiers] = "modifiers";
  roles[EntValue] = "value";
  roles[EntValueType] = "valuetype";
  roles[PeerName] = "peername";
  return roles;
}

QVariant EntityModel::data(const QModelIndex &index, int role) const
{
  QVariant retVal;
  switch (role) {
    case EntName:
      retVal=dataTable.value(index.row())->getName();
      break;
    case EntModifiers:
      retVal=QString(prettyModifierNames(dataTable.value(index.row())->getModifiers()).join(", "));
      break;
    case EntValue:
      retVal=dataTable.value(index.row())->getValue();
      break;
    case EntValueType:
      retVal=dataTable.value(index.row())->getValue().typeName();
      break;
    case PeerName:
      retVal=dataTable.value(index.row())->getOwner()->getName();
      break;
    default:
      break;
  }

  return retVal;
}

int EntityModel::rowCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent)
  return dataTable.count();
}

QUrl EntityModel::src() const
{
  return m_src;
}

void EntityModel::setSrc(QUrl url)
{
  if (m_src.isEmpty()) {

    if(url.isValid() && url.scheme().toLower()=="vein" && !url.hasFragment())
    {
      VeinHub *tmpHub=0;
      m_src = url;
      emit srcChanged(url);
      tmpHub = VeinService::getInstance()->resolveURLToHub(url);
      if(tmpHub)
      {
        m_hub=tmpHub;
        alignDataTable();
      }
      else
      {
        connect(VeinService::getInstance(),&VeinService::sigHubConnected,this,&EntityModel::onHubConnected);
      }
    }
    else
    {
      qWarning() << "[vein-qml] EntityModel.src URL mismatch: " << url.toDisplayString() << "\n-\nExpected:\n  vein://ipaddress:port/ \nOR\n  vein://127.0.0.1/ (for the local VeinHub)";
    }
  }
}

void EntityModel::onHubConnected(VeinHub *newHub)
{
  if(newHub)
  {
    ProtoNetPeer *tmpPeer=VeinService::getInstance()->getController()->getRemoteConnectionFromHub(newHub);
    bool localhost = tmpPeer->getIpAddress()=="127.0.0.1" && m_src.host()=="localhost";
    if((localhost || tmpPeer->getIpAddress()==m_src.host()) && tmpPeer->getPort()==m_src.port())
    {
      m_hub=newHub;
      alignDataTable();
      disconnect(VeinService::getInstance(),&VeinService::sigHubConnected,this,&EntityModel::onHubConnected);
    }
  }
}

void EntityModel::onEntValueChanged()
{
  VeinEntity *tmpEnt = qobject_cast<VeinEntity *>(sender());
  int entId = -1;
  for(int entCount=0; entCount<dataTable.size() && entId<0; entCount++)
  {
    if(dataTable.value(entCount)==tmpEnt)
    {
      entId = entCount;
    }
  }
  dataChanged(createIndex(entId,0),createIndex(entId,0),QVector<int>()<<EntValue);
}

void EntityModel::onEntModifiersChanged()
{
  VeinEntity *tmpEnt = qobject_cast<VeinEntity *>(sender());
  int entId = -1;
  for(int entCount=0; entCount<dataTable.size() && entId<0; entCount++)
  {
    if(dataTable.value(entCount)==tmpEnt)
    {
      entId = entCount;
    }
  }
  dataChanged(createIndex(entId,0),createIndex(entId,0),QVector<int>()<<EntModifiers);
}

void EntityModel::onEntAdded()
{
  alignDataTable();
}

void EntityModel::onEntRemoved()
{
  alignDataTable();
}

void EntityModel::alignDataTable()
{
  beginResetModel();
  dataTable.clear();
  endResetModel();
  int modelCount=0;

  QList<VeinPeer*> pList=m_hub->listPeers();
  for(int peerCount=0; peerCount<pList.count(); peerCount++)
  {
    VeinPeer *tmpPeer=pList.at(peerCount);
    QList<VeinEntity *> eList=tmpPeer->getEntityList();
    for(int entCount=0; entCount<eList.count(); entCount++)
    {
      VeinEntity *tmpEnt = eList.at(entCount);
      dataTable.insert(modelCount,tmpEnt);
      ///@note this function can be called multiple times for the same pointers, therefore Qt::UniqueConnection was used
      connect(tmpEnt,SIGNAL(sigValueChanged(QVariant)),this,SLOT(onEntValueChanged()),Qt::UniqueConnection);
      connect(tmpEnt,SIGNAL(sigModifiersChanged(QList<Modifiers>)),this,SLOT(onEntModifiersChanged()),Qt::UniqueConnection);
      tmpEnt->fetchEntityData();
      modelCount++;
    }
    connect(tmpPeer,SIGNAL(sigEntityAdded(VeinEntity*)),this,SLOT(onEntAdded()),Qt::UniqueConnection);
    connect(tmpPeer,SIGNAL(sigEntityRemoved(VeinEntity*)),this,SLOT(onEntRemoved()),Qt::UniqueConnection);
  }

  beginInsertRows(QModelIndex(),0,dataTable.count()-1);
  endInsertRows();
}

QStringList EntityModel::prettyModifierNames(QList<VeinEntity::Modifiers> mods) const
{
  QStringList retVal;
  for(int modCount=0; modCount<mods.count(); modCount++)
  {
    switch (mods.at(modCount)) {
      case VeinEntity::MOD_CONST:
        retVal<<"MOD_CONST";
        break;
      case VeinEntity::MOD_NOECHO:
        retVal<<"MOD_NOECHO";
        break;
      case VeinEntity::MOD_READONLY:
        retVal<<"MOD_READONLY";
        break;
    }
  }
  return retVal;
}
