#ifndef VEINHUB_H
#define VEINHUB_H

#include <QQuickItem>

class Peer;
class VeinHub;

class Hub : public QQuickItem
{
  Q_OBJECT
  Q_DISABLE_COPY(Hub)

public:
  Hub(QQuickItem *parent = 0);
  ~Hub();

  Q_INVOKABLE Peer *getPeer(QString name);
  Q_INVOKABLE QList<Peer *> listPeers();

  Q_INVOKABLE Peer *peerAdd(QString name);
  Q_INVOKABLE void peerRemove(Peer *peer);

signals:
  void peerRegistered(Peer *peer);

private:
  VeinHub *hub;
};

#endif // VEINHUB_H

