#include "zbarscaledraw.h"

#include <qwt_text.h>

ZBarScaleDraw::ZBarScaleDraw() : QwtScaleDraw()
{
}

ZBarScaleDraw::ZBarScaleDraw(Qt::Orientation orientation, const QStringList &labels) : d_labels(labels)
{
  setTickLength(QwtScaleDiv::MinorTick, 0);
  setTickLength(QwtScaleDiv::MediumTick, 0);
  setTickLength(QwtScaleDiv::MajorTick, 2);

  enableComponent(QwtScaleDraw::Backbone, false);

  if (orientation == Qt::Vertical)
  {
    setLabelRotation(-60.0);
  }
  else
  {
    setLabelRotation(-20.0);
  }

  setLabelAlignment(Qt::AlignLeft | Qt::AlignVCenter);
}

void ZBarScaleDraw::setColor(QColor arg)
{
  textColor=arg;
}

QwtText ZBarScaleDraw::label(double value) const
{
  QwtText lbl;
  if(d_labels.count()>value)
  {
    const int index = qRound(value);
    if (index >= 0 && index < d_labels.size())
    {
      lbl = d_labels.at(index);
    }
  }
  else
  {
    lbl = QwtText (QString::number (value));
  }
  lbl.setColor(textColor);
  return lbl;
}
