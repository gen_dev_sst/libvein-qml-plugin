#ifndef ZBARSCALEDRAW_H
#define ZBARSCALEDRAW_H

#include <qwt_scale_draw.h>
#include <QColor>
#include <QStringList>

class ZBarScaleDraw : public QwtScaleDraw
{
public:
  ZBarScaleDraw();
  ZBarScaleDraw(Qt::Orientation orientation, const QStringList &labels);

  void setColor(QColor arg);

  virtual QwtText label(double value) const Q_DECL_OVERRIDE;

private:
  QColor textColor;
  QStringList d_labels;
};

#endif // ZBARSCALEDRAW_H
