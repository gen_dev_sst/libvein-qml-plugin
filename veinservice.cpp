#include "veinservice.h"

#include "entity.h"

#include <veintcpcontroller.h>
#include <veinhub.h>
#include <veinpeer.h>
#include <veinentity.h>
#include <protonetpeer.h>

#include <QUrl>
#include <QPointer>
#include <QDebug>

VeinService *VeinService::singletonInstance=0;

VeinService::VeinService(QObject *qobjParent) :
  QObject(qobjParent),
  controller(new VeinTcpController(this))
{
  controller->getLocalHub()->setUuid(QUuid::createUuid());

  connect(controller,SIGNAL(sigHubCreated(VeinHub*)),this,SLOT(onHubConnected(VeinHub*)));
  connect(controller,SIGNAL(sigSocketError()),this,SLOT(onSocketError()));
  //controller->startService(9001); //in case of p2p connections, but currently unused

}

VeinService::~VeinService()
{
  // QObject parenting system will delete the controller automatically
  if(this==singletonInstance)
  {
    singletonInstance=0;
  }
  qDebug() << "[VeinQML]DELETED" << this;
}

bool VeinService::checkWaiting(QUrl url)
{
  bool retVal=false;
  for(int i=0; i<networkWaiting.count(); i++)
  {
    Entity *tmpEntity = networkWaiting.at(i);
    if(tmpEntity->src().host()==url.host() && tmpEntity->src().port()==url.port())
    {
      retVal=true;
      break;
    }
  }
  return retVal;
}

VeinEntity *VeinService::resolveURL(const QUrl &url)
{
  VeinHub *hub = 0;
  VeinPeer *peer = 0;
  VeinEntity *retVal = 0;

  if(url.port()>0)
  {
    QList<ProtoNetPeer *> netPeers = controller->getPeerlist();
    for(int i=0; i<netPeers.count(); i++) //search for already opened connections
    {
      if(netPeers.at(i)->getIpAddress()==url.host() && netPeers.at(i)->getPort()==url.port())
      {
        hub = controller->getRemoteHubFromConnection(netPeers.at(i));
        break;
      }
    }
    if(!hub) //no connection to this host on that port found so connect to it
    {
      if(!checkWaiting(url))
      {
        controller->connectToServer(url.host(),url.port());
      }
    }
  }
  else if(url.host().contains("localhost", Qt::CaseInsensitive) || url.host()=="127.0.0.1") /// @todo add check for IPV6 localhost
  {
    hub = controller->getLocalHub();
  }

  if(hub)
  {
    //we do not want the leading "/" character so remove it
    QString tmpPath=url.path(QUrl::FullyDecoded).mid(1);

    peer = hub->getPeerByName(tmpPath);
    if(peer)
    {
      retVal=peer->getEntityByName(url.fragment(QUrl::FullyDecoded));
    }
    else
    {
      qWarning() << "[vein-qml] Could not find VeinPeer: "<<tmpPath;
    }
  }
  return retVal;
}

VeinHub *VeinService::resolveURLToHub(const QUrl &url)
{
  VeinHub *hub = 0;
  if(url.port()>0)
  {
    QList<ProtoNetPeer *> netPeers = controller->getPeerlist();
    for(int i=0; i<netPeers.count(); i++) //search for already opened connections
    {
      if(netPeers.at(i)->getIpAddress()==url.host() && netPeers.at(i)->getPort()==url.port())
      {
        hub = controller->getRemoteHubFromConnection(netPeers.at(i));
        break;
      }
    }
    if(!hub && !checkWaiting(url)) //no connection to this host on that port found so connect to it
    {
      controller->connectToServer(url.host(),url.port());
    }
  }
  else if(url.host().contains("localhost", Qt::CaseInsensitive) || url.host()=="127.0.0.1") /// @todo add check for IPV6 localhost
  {
    hub = controller->getLocalHub();
  }
  return hub;
}

VeinService *VeinService::getInstance()
{
  if(singletonInstance==0)
  {
    singletonInstance=new VeinService();
  }
  return singletonInstance;
}

void VeinService::deconstructSingleton()
{
  if(singletonInstance!=0)
  {
    singletonInstance->deleteLater();
    singletonInstance = 0;
  }
}

void VeinService::addWaiting(Entity *ent)
{
  networkWaiting.append(ent);
}

VeinTcpController *VeinService::getController()
{
  return controller;
}

void VeinService::onHubConnected(VeinHub *remoteHub)
{
  sigHubConnected(remoteHub);
  //do some debug introspection
  QList<VeinPeer*> pList=remoteHub->listPeers();
  qDebug() << "\n[H]" << remoteHub->getUuid();
  for(int i=0; i<pList.count(); i++)
  {
    qDebug() << "\n[P]" << pList.at(i)->getName();
  }

  //check if host+port matches any listed Vein instances
  for(int i=0; i<networkWaiting.count(); i++)
  {
    Entity *waitingEntity = networkWaiting.at(i);
    ProtoNetPeer *remoteConnection=controller->getRemoteConnectionFromHub(remoteHub);
    if(remoteConnection)
    {
      if(waitingEntity->src().port() == remoteConnection->getPort())
      {
        //check if the peer and entity exists
        VeinPeer *tmpPeer = remoteHub->getPeerByName(waitingEntity->src().path().mid(1)); // ignore the first "/"
        if(tmpPeer)
        {
          QPointer<VeinEntity> tmpEntity = tmpPeer->getEntityByName(waitingEntity->src().fragment(QUrl::FullyDecoded));
          if(tmpEntity)
          {
            waitingEntity->remoteUrlResolved(tmpEntity);
            //do not break the loop because other entities might wait for the same hub
          }
          else
          {
            qWarning() << "[vein-qml] Target VeinHub does not provide any VeinEntity named: "<<waitingEntity->src().fragment(QUrl::FullyDecoded) << "\n- Context:"<<waitingEntity->src();
          }
        }
        else
        {
          qWarning() << "[vein-qml] Target VeinHub does not provide any VeinPeer named: "<<waitingEntity->src().path().mid(1) << "\n- Context:"<<waitingEntity->src();
        }
      }
    }
  }
}

void VeinService::onSocketError()
{
  ///@todo test
  qDebug() << "[VeinQML]Socket error" << this;
  emit sigSocketError();
}
