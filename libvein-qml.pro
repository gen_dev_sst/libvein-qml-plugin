TEMPLATE = lib

exists( ../include/project-paths.pri ) {
  include(../include/project-paths.pri)
}
else:exists( ../../project-paths.pri ) {
  include(../../project-paths.pri)
} else:exists(../gui-session.pri) {
  include(../gui-session.pri)
}

TARGET = VeinQML
QT += qml quick
CONFIG += qt plugin

TARGET = $$qtLibraryTarget($$TARGET)
uri = Vein

INCLUDEPATH += $$VEIN_TCP_INCLUDEDIR
LIBS += $$VEIN_TCP_LIBDIR -lvein-tcp-overlay

INCLUDEPATH += $$VEIN_INCLUDEDIR
LIBS += $$VEIN_LIBDIR -lvein-qt

INCLUDEPATH += $$PROTONET_INCLUDEDIR
LIBS += $$PROTONET_LIBDIR -lproto-net-qt


# Input
SOURCES += \
    entity.cpp \
    veinqml_plugin.cpp \
    veinservice.cpp \
    entitymodel.cpp \
    zfftbarchart.cpp \
    zbarscaledraw.cpp \
    zbardata.cpp

HEADERS += \
    entity.h \
    veinqml_plugin.h \
    veinservice.h \
    entitymodel.h \
    zfftbarchart.h \
    zbarscaledraw.h \
    zbardata.h

OTHER_FILES = qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) \"$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)\" \"$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)\"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir
unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(uri, \\., /)
    qmldir.path = $$installPath
    target.path = $$installPath
    INSTALLS += target qmldir
}

QT += widgets

INCLUDEPATH += $$QWT_INCLUDEDIR
LIBS += $$QWT_LIBDIR -lqwt

