#include "zbardata.h"

#include <qwt_column_symbol.h>
#include <qwt_text.h>
#include <QPalette>

ZBarData::ZBarData()
{
  setLegendMode(QwtPlotBarChart::LegendBarTitles);
  setLegendIconSize(QSize(10, 14));
  setSpacing(4);
}

void ZBarData::addData(QColor col, QString title)
{
  colors += col;
  titles += title;
  itemChanged();
}

QwtColumnSymbol *ZBarData::specialSymbol(int sampleIndex, const QPointF &) const
{
  QwtColumnSymbol *symbol = new QwtColumnSymbol(QwtColumnSymbol::Box);
  symbol->setLineWidth(2);
  symbol->setFrameStyle(QwtColumnSymbol::Raised);

  QColor c(Qt::white);
  if (sampleIndex >= 0 && sampleIndex < colors.size())
    c = colors[ sampleIndex ];

  symbol->setPalette(QPalette(c));

  return symbol;
}

QwtText ZBarData::barTitle(int sampleIndex) const
{
  QwtText title;
  if (sampleIndex >= 0 && sampleIndex < titles.size())
    title = titles[ sampleIndex ];
  return title;
}

void ZBarData::clearData()
{
  //qDebug("CLEAR");
  colors.clear();
  titles.clear();
  dataChanged();
}

QStringList ZBarData::getTitles()
{
  return titles;
}
