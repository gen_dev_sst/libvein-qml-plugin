#ifndef ZBARDATA_H
#define ZBARDATA_H

#include <qwt_plot_barchart.h>
#include <QColor>
#include <QString>
#include <QList>

class ZBarData : public QwtPlotBarChart
{

public:
  ZBarData();
  void addData(QColor col, QString title);

  void clearData();

  QStringList getTitles();
  // QwtPlotBarChart interface
public:
  virtual QwtColumnSymbol *specialSymbol(int sampleIndex, const QPointF &) const Q_DECL_OVERRIDE;
  QwtText barTitle(int sampleIndex) const Q_DECL_OVERRIDE;



private:
  QList<QColor> colors;
  QList<QString> titles;
};

#endif // ZBARDATA_H
