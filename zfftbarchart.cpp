#include "zfftbarchart.h"

#include <QTimer>

#include <qwt_plot_renderer.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_barchart.h>
#include <qwt_column_symbol.h>
#include <qwt_plot_layout.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_scale_engine.h>
#include <qwt_plot.h>
#include <qwt_scale_widget.h>
#include <qwt_text.h>

#include <veinentity.h>

#include "zbardata.h"
#include "zbarscaledraw.h"
#include "veinservice.h"

ZFFTBarChart::ZFFTBarChart(QQuickItem *parent):
  QQuickPaintedItem(parent),
  m_refreshTimer(new QTimer(this)),
  m_valuesTimer(new QTimer(this)),
  m_canvas(new QwtPlotCanvas()),
  m_plot(new QwtPlot()),
  m_barDataLeft(new ZBarData()),
  m_minValueLeftAxis(1.0),
  m_leftEntity(0),
  m_leftBarCount(0),
  m_barDataRight(new ZBarData()),
  m_minValueRightAxis(1.0),
  m_rightEntity(0)
{
  connect(this, SIGNAL(heightChanged()), this, SLOT(onHeightChanged()));
  connect(this, SIGNAL(widthChanged()), this, SLOT(onWidthChanged()));
  connect(this, SIGNAL(labelsChanged(QStringList)), this, SLOT(onLabelsChanged(QStringList)));

  connect(m_refreshTimer,SIGNAL(timeout()),this,SLOT(onRefreshTimeout()));
  connect(m_valuesTimer,SIGNAL(timeout()),this,SLOT(onExternValuesChangedTimeout()));

  m_plot->setAttribute(Qt::WA_NoSystemBackground);
  m_plot->setAutoFillBackground(true);

  m_canvas->setLineWidth(1);
  m_canvas->setFrameStyle(QFrame::NoFrame);

  m_plot->setAxisScaleDraw(QwtPlot::yLeft, new ZBarScaleDraw());
  m_plot->setAxisScaleDraw(QwtPlot::yRight, new ZBarScaleDraw());
  m_plot->setAxisScaleDraw(QwtPlot::xBottom, new ZBarScaleDraw());

  m_plot->setCanvas(m_canvas);

  m_barDataLeft->attach(m_plot);
  m_barDataLeft->setYAxis(QwtPlot::yLeft);

  m_barDataRight->attach(m_plot);
  m_barDataRight->setYAxis(QwtPlot::yRight);

  m_plot->setAutoReplot(true);

  //plot->setAxisScaleEngine(QwtPlot::yLeft, new QwtLogScaleEngine);
  //plot->setAxisScale(QwtPlot::yLeft, 1, 10000);
}

ZFFTBarChart::~ZFFTBarChart()
{
  delete m_canvas;
  delete m_plot;
}

bool ZFFTBarChart::bottomLabels() const
{
  return m_bottomLabelsEnabled;
}

bool ZFFTBarChart::legendEnabled() const
{
  return m_legendEnabled;
}

QColor ZFFTBarChart::bgColor() const
{
  return m_bgColor;
}

QColor ZFFTBarChart::borderColor() const
{
  return m_borderColor;
}

QColor ZFFTBarChart::textColor() const
{
  return m_textColor;
}

QString ZFFTBarChart::chartTitle() const
{
  return m_chartTitle;
}

void ZFFTBarChart::componentComplete()
{
  onRefreshTimeout();
}

void ZFFTBarChart::paint(QPainter *painter)
{
  painter->setRenderHints(QPainter::Antialiasing, true);
  m_plot->render(painter);
}



bool ZFFTBarChart::logScaleLeftAxis() const
{
  return m_logScaleLeftAxis;
}

double ZFFTBarChart::maxValueLeftAxis() const
{
  return m_maxValueLeftAxis;
}

double ZFFTBarChart::minValueLeftAxis() const
{
  return m_minValueLeftAxis;
}

QColor ZFFTBarChart::colorLeftAxis() const
{
  return m_colorLeftAxis;
}

QString ZFFTBarChart::titleLeftAxis() const
{
  return m_plot->axisTitle(QwtPlot::yLeft).text();
}

QUrl ZFFTBarChart::leftValueSource() const
{
  return m_leftValueSource;
}

bool ZFFTBarChart::logScaleRightAxis() const
{
  return m_logScaleRightAxis;
}

double ZFFTBarChart::maxValueRightAxis() const
{
  return m_maxValueRightAxis;
}

double ZFFTBarChart::minValueRightAxis() const
{
  return m_minValueRightAxis;
}

QColor ZFFTBarChart::colorRightAxis() const
{
  return m_colorRightAxis;
}

bool ZFFTBarChart::rightAxisEnabled() const
{
  return m_plot->axisEnabled(QwtPlot::yRight);
}

QString ZFFTBarChart::titleRightAxis() const
{
  return m_plot->axisTitle(QwtPlot::yLeft).text();
}

QUrl ZFFTBarChart::rightValueSource() const
{
  return m_rightValueSource;
}

void ZFFTBarChart::onExternValuesChanged()
{
  if(!m_valuesTimer->isActive())
    m_valuesTimer->start(100);
}

void ZFFTBarChart::onHeightChanged()
{
  if(contentsBoundingRect().height()>0)
  {
    m_plot->setFixedHeight(contentsBoundingRect().height());
  }
  else
  {
    m_plot->setFixedHeight(0);
  }
  refreshPlot();
}

void ZFFTBarChart::onWidthChanged()
{
  if(contentsBoundingRect().width()>0)
  {
    m_plot->setFixedWidth(contentsBoundingRect().width());
  }
  else
  {
    m_plot->setFixedWidth(0);
  }
  refreshPlot();
}

void ZFFTBarChart::setBgColor(QColor arg)
{
  if (m_bgColor != arg) {
    QPalette p = m_plot->palette();
    p.setColor(QPalette::Window, arg);
    m_canvas->setPalette(p);
    m_bgColor = arg;
    emit bgColorChanged(arg);
  }
}

void ZFFTBarChart::setborderColor(QColor arg)
{
  if (m_borderColor != arg) {
    m_borderColor = arg;

    /// @todo Broken TBD
    emit borderColorChanged(arg);
  }
}

void ZFFTBarChart::setBottomLabels(bool arg)
{
  m_bottomLabelsEnabled=arg;
  if(arg)
  {
    m_plot->setAxisScaleDraw(QwtPlot::xBottom, new ZBarScaleDraw(Qt::Vertical,m_bottomLabels));
    m_plot->setAxisMaxMajor(QwtPlot::xBottom,m_bottomLabels.count());
    refreshPlot();
  }
  else
  {
    m_bottomLabels.clear();
    m_plot->setAxisScaleDraw(QwtPlot::xBottom, new ZBarScaleDraw());
    refreshPlot();
  }
}

void ZFFTBarChart::setChartTitle(QString arg)
{
  if (m_chartTitle != arg) {
    m_chartTitle = arg;
    m_plot->setTitle(arg);
  }
}

void ZFFTBarChart::setLegendEnabled(bool arg)
{
  if(arg!=m_legendEnabled)
  {
    if(arg)
    {
      QwtLegend *tmpLegend = new QwtLegend();
      QPalette tmpPa;
      tmpPa.setColor(QPalette::Text,arg);
      tmpPa.setColor(QPalette::WindowText,arg);
      tmpPa.setColor(QPalette::Window,Qt::transparent);
      tmpPa.setColor(QPalette::Base,Qt::transparent);

      tmpLegend->setPalette(tmpPa);
      m_plot->insertLegend(tmpLegend);
    }
    else
    {
      m_plot->insertLegend(NULL);
    }
    refreshPlot();
  }
  m_legendEnabled=arg;
}

void ZFFTBarChart::setTextColor(QColor arg)
{

  if(arg != m_textColor)
  {
    ZBarScaleDraw *tmpScaleX;
    QPalette tmpPa;
    tmpPa.setColor(QPalette::Text,arg);
    tmpPa.setColor(QPalette::WindowText,arg);
    tmpPa.setColor(QPalette::Window,Qt::transparent);
    tmpPa.setColor(QPalette::Base,Qt::transparent);

    m_plot->setPalette(tmpPa);

    if(m_plot->legend())
    {
      m_plot->legend()->setPalette(tmpPa);
    }

    //plot->axisWidget(QwtPlot::yLeft)->setPalette(tmpPa);
    m_plot->axisWidget(QwtPlot::xBottom)->setPalette(tmpPa);

    tmpScaleX=new ZBarScaleDraw();
    tmpScaleX->setColor(arg);

    //tmpScaleY=new ZBarScaleDraw();
    //tmpScaleY->setColor(arg);

    ///todo check if this is necessary since the palette was set previously
    m_plot->setAxisScaleDraw(QwtPlot::xBottom,tmpScaleX);

    labelsChanged(m_bottomLabels);

    refreshPlot();
  }

  m_textColor = arg;
}

void ZFFTBarChart::setLogScaleLeftAxis(bool arg)
{
  if(arg!=m_logScaleLeftAxis)
  {
    m_logScaleLeftAxis = arg;
    if(arg)
    {
      m_plot->setAxisScaleEngine(QwtPlot::yLeft, new QwtLogScaleEngine);
    }
    else
    {
      m_plot->setAxisScaleEngine(QwtPlot::yLeft, new QwtLinearScaleEngine);
    }
    m_plot->setAxisScale(QwtPlot::yLeft, m_minValueLeftAxis, m_maxValueLeftAxis);
    m_barDataLeft->setBaseline(m_minValueLeftAxis/10);
    //calculate optimised scale min/max
    setMaxValueLeftAxis(m_maxValueLeftAxis);
    setMinValueLeftAxis(m_minValueLeftAxis);
    refreshPlot();
  }
}

void ZFFTBarChart::setMaxValueLeftAxis(double arg)
{
  double tmpScale=1;
  if(m_logScaleLeftAxis)
  {
    while(tmpScale<arg)
    {
      tmpScale=tmpScale*10;
    }
  }
  else
  {
    tmpScale=arg;
  }
  m_plot->setAxisScale(QwtPlot::yLeft, m_minValueLeftAxis, tmpScale);
  m_maxValueLeftAxis = arg;
  refreshPlot();
}

void ZFFTBarChart::setMinValueLeftAxis(double arg)
{
  double tmpScale=1;
  if(m_logScaleLeftAxis)
  {
    while(tmpScale>arg)
    {
      tmpScale=tmpScale/10;
    }
  }
  else
  {
    tmpScale=arg;
  }
  m_plot->setAxisScale(QwtPlot::yLeft, tmpScale, m_maxValueLeftAxis);
  m_minValueLeftAxis = arg;
  refreshPlot();
}

void ZFFTBarChart::setColorLeftAxis(QColor arg)
{
  QPalette tmpPa;
  tmpPa.setColor(QPalette::Text,arg);
  tmpPa.setColor(QPalette::WindowText,arg);
  tmpPa.setColor(QPalette::Window,Qt::transparent);
  tmpPa.setColor(QPalette::Base,Qt::transparent);

  m_plot->axisWidget(QwtPlot::yLeft)->setPalette(tmpPa);
  m_colorLeftAxis = arg;
}

void ZFFTBarChart::setTitleLeftAxis(QString arg)
{
  m_plot->setAxisTitle(QwtPlot::yLeft, arg);
}

void ZFFTBarChart::setLeftValueSource(const QUrl &leftValueEntity)
{
  if (m_leftValueSource == leftValueEntity)
    return;

  m_leftValueSource = leftValueEntity;
  m_rightEntity = VeinService::getInstance()->resolveURL(m_leftValueSource);
  if(m_rightEntity!=0)
  {
    connect(m_rightEntity, &VeinEntity::sigValueChanged, this, &ZFFTBarChart::onLeftValueChanged);
  }
  emit leftValueSourceChanged(leftValueEntity);
}

void ZFFTBarChart::setLogScaleRightAxis(bool arg)
{
  if(arg!=m_logScaleRightAxis)
  {
    m_logScaleRightAxis = arg;
    if(arg)
    {
      m_plot->setAxisScaleEngine(QwtPlot::yRight, new QwtLogScaleEngine);
    }
    else
    {
      m_plot->setAxisScaleEngine(QwtPlot::yRight, new QwtLinearScaleEngine);
    }
    m_plot->setAxisScale(QwtPlot::yRight, m_minValueRightAxis, m_maxValueRightAxis);
    m_barDataRight->setBaseline(m_minValueRightAxis/10);
    //calculate optimised scale min/max
    setMaxValueRightAxis(m_maxValueRightAxis);
    setMinValueRightAxis(m_minValueRightAxis);
    refreshPlot();
  }
}

void ZFFTBarChart::setMaxValueRightAxis(double arg)
{
  double tmpScale=1;
  if(m_logScaleRightAxis)
  {
    while(tmpScale<arg)
    {
      tmpScale=tmpScale*10;
    }
  }
  else
  {
    tmpScale=arg;
  }
  m_plot->setAxisScale(QwtPlot::yRight, m_minValueRightAxis, tmpScale);
  m_maxValueRightAxis = arg;
  refreshPlot();
}

void ZFFTBarChart::setMinValueRightAxis(double arg)
{
  double tmpScale=1;
  if(m_logScaleRightAxis)
  {
    while(tmpScale>arg)
    {
      tmpScale=tmpScale/10;
    }
  }
  else
  {
    tmpScale=arg;
  }
  m_plot->setAxisScale(QwtPlot::yRight, tmpScale, m_maxValueRightAxis);
  m_minValueRightAxis = arg;
  refreshPlot();
}

void ZFFTBarChart::setColorRightAxis(QColor arg)
{
  QPalette tmpPa;
  tmpPa.setColor(QPalette::Text,arg);
  tmpPa.setColor(QPalette::WindowText,arg);
  tmpPa.setColor(QPalette::Window,Qt::transparent);
  tmpPa.setColor(QPalette::Base,Qt::transparent);

  m_plot->axisWidget(QwtPlot::yRight)->setPalette(tmpPa);
  m_colorRightAxis = arg;
}

void ZFFTBarChart::setRightAxisEnabled(bool arg)
{
  m_plot->enableAxis(QwtPlot::yRight, arg);
}

void ZFFTBarChart::setTitleRightAxis(QString arg)
{
  m_plot->setAxisTitle(QwtPlot::yRight, arg);
}

void ZFFTBarChart::setRightValueSource(const QUrl &rightValueSource)
{
  if (m_rightValueSource == rightValueSource)
    return;

  m_rightValueSource = rightValueSource;

  m_rightEntity = VeinService::getInstance()->resolveURL(m_rightValueSource);
  if(m_rightEntity!=0)
  {
    connect(m_rightEntity, &VeinEntity::sigValueChanged, this, &ZFFTBarChart::onRightValueChanged);
  }
  emit rightValueSourceChanged(rightValueSource);
}

void ZFFTBarChart::onExternValuesChangedTimeout()
{
  QVector<double> tmpSamples;
  int loopCount=0;
  const float tmpScaleFactor = m_maxValueLeftAxis/m_maxValueRightAxis;

  m_valuesTimer->stop();

  if(m_valuesLeftAxis.count()>0 && m_valuesLeftAxis.count() == m_valuesRightAxis.count())
  {
    //m_valuesLeftAxis is a list of mixed real and imaginary numbers
    //and m_valuesRightAxis needs to be mudballed into the samples
    loopCount = m_valuesLeftAxis.count();

    if(m_leftBarCount != loopCount)
    {
      m_leftBarCount = loopCount;
      onLeftBarCountChanged(m_leftBarCount);
    }


    for(int i=0; i<loopCount-1;)
    {
      QVector2D tmpVectorA ,tmpVectorB;


      tmpVectorA.setX(m_valuesLeftAxis.at(i));
      tmpVectorA.setY(m_valuesLeftAxis.at(i+1));

      tmpSamples.append(tmpVectorA.length());

      tmpVectorB.setX(m_valuesRightAxis.at(i));
      tmpVectorB.setY(m_valuesRightAxis.at(i+1));

      //this is bullshit, but due to management decisions it is required
      tmpSamples.append(tmpVectorB.length()*tmpScaleFactor);
      i+=2;
    }
    if(m_legendEnabled)
      m_plot->insertLegend(new QwtLegend());
    labelsChanged(m_barDataLeft->getTitles());
  }

  m_barDataLeft->setSamples(tmpSamples);
}

void ZFFTBarChart::onLabelsChanged(QStringList arg)
{
  m_bottomLabels=arg;
  setBottomLabels(m_bottomLabelsEnabled);
  refreshPlot();
}

void ZFFTBarChart::onRefreshTimeout()
{
  m_refreshTimer->stop();
  //qDebug("UPDATE");
  m_plot->updateGeometry();
  m_plot->updateAxes();
  m_plot->updateLegend();
  m_plot->updateLayout();
  m_plot->updateCanvasMargins();
  this->update();
}

void ZFFTBarChart::refreshPlot()
{
  //when resizing is in progress it may not be suitable to refresh for every pixel changed in width or height
  if(!m_refreshTimer->isActive())
    m_refreshTimer->start(500);
}

void ZFFTBarChart::onLeftValueChanged(QVariant leftValue)
{
  m_valuesLeftAxis = leftValue.value<QList<qreal>>();
  onExternValuesChanged();
}

void ZFFTBarChart::onRightValueChanged(QVariant rightValue)
{
  m_valuesRightAxis = rightValue.value<QList<qreal>>();
  onExternValuesChanged();
}

void ZFFTBarChart::onLeftBarCountChanged(int barCount)
{
  QString tmpBarTitle;

  m_barDataLeft->clearData();

  for(int i=0; i<barCount-1;)
  {
    if(i%2 == 0)
    {
      tmpBarTitle=QString::number(qAbs(i/2));
    }
    else
    {
      tmpBarTitle=QString();
    }
    m_barDataLeft->addData(m_colorLeftAxis, tmpBarTitle);
    m_barDataLeft->addData(m_colorRightAxis, QString(""));
    i+=2;
  }
}
