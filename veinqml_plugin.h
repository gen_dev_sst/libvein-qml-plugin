#ifndef LIBVEIN_QML_PLUGIN_H
#define LIBVEIN_QML_PLUGIN_H

#include <QQmlExtensionPlugin>

class VeinQMLPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "Vein")

public:
  void registerTypes(const char *uri);
};

#endif // LIBVEIN_QML_PLUGIN_H

